# WordPress

Mosaic's Website runs WordPress, a content-management system.

While it is very user-friendly, there are some tasks which can seem confusing to someone who does not have experience with using WordPress. This documentation seeks to fill those gaps.

- [Administrative Tasks](admin.md) - How to perform administrative tasks
- [Events Manager](events.md) - How to use the Events Manager
- [Media Browser](media.md) - How to manage Media
- [Mini Tasks](mini-tasks.md) - small tasks that don't belong to any particular category
- [Pages](page.md) - How to edit Pages
- [Posts](post.md) - How to edit Posts
- [Services](services.md) - How to manage Sunday (or other) Services
- [Tesimonials](testimonials.md) - How to manage Testimonials
