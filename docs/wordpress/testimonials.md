# Testimonials

This page covers the Testimonials plugin. It will also contain any custom css snippets related to display of testimonials.

## Testimonial Content Guidelines

Testimonials should be divided into readable paragraphs (No "wall of text", please!). The content of the first paragraph should be a short summary to keep the footer testimonial slider from being too tall. Subsequent paragraphs may go into more detail.

## Add a new testimonial

1. Sign in to the Mosaic website.
2. Find "Testimonials" near the middle of the left-hand-side menu in the Admin Dashboard. 
    1. Click "All Testimonials"
3. Click "add new" in the upper left hand side of the screen.
4. Enter the author's name of the testimonial in the "title" box.
5. Enter the testimonial text in the next box just below the "title" box.
6. Click "publish" on the top right hand side of the menu bar. 

## Edit an Existing Testimonial

1. Sign into the Mosaic website.
2. Find "Testimonials" near the middle of the left-hand-side menu in the Admin Dashboard. 
    1. Click "All Testimonials"
3. Find the Testimonial you wish to edit and click on the author's name.
4. Make your edits.
5. When you are satisfied, click "Update" in the upper right corner.

## Delete a Testimonial
1. Sign in to Mosaics website.
2. Go to testimonials near the middle right hand side of the screen in the left hand side menu. Click "all testimonials"
3. Choose which testimonial you would like to delete.
4. Hover over the box of that testimonial and click "trash" to delete.
