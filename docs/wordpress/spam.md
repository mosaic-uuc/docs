# Comment Moderation

Even with spam prevention tools, occasionally a spammy comment may slip through. Here's how to deal with comments.

## Identifying Spam Comments

Some comment spammers try to sneak in by posting something bland and non-offensive in order to get their website url to appear with the comment. We can do two things: not display commenter website URLs (set in the settings), and ruthlessly delete comments with no substance.

Example comment:

>Hello my friend! I wish to say that this article is awesome, nice written and include appoximately all vital infos. I would like to see extra posts like this .

Other comments may include sentence fragments:

>It’s actualⅼy very complex in this fuⅼl
>of activity life to listen news on ƬV, therefore I ϳust use web for thаt reason, and take the most recent information.

## Improve the Filter Before Deleting

Before deleting comments, it's useful to examine them for similarities, and use that information to add auto-block criteria to prevent those spammers from returning. All of the comment spam you might see in a single review may include the same URL. If you find something common among the spam comments that seems unique, you can add it to the `Disallowed Comment Keys` field in `Settings -> Discussion`, one key per line.

## Removing Spam Comments

All comments from authors who don't have a prior approved comment are held for moderation. This prevents spam from appearing on the website by placing a human (or humans) in between the spammer and the website. Anyone who has the appropriate level of admin access may log in and review comments.

In `WP Admin -> Comments`:

- Hover over a single comment in the row. Some option links will appear below the comment body. 
  - If the comment is spam (including bland spam), click "Spam"; the comment will be marked as Spam
- If you have many comments to deal with at once, you can select multiple by checking each row's checkbox, then use the Bulk Actions menu at the top of the table to select the action, then click "Apply" and the action will be done to all selected comments at once.

## Approving Legitimate Comments

Legitimate comments are rare these days, but they do happen.

In `WP Admin -> Comments`:

- Hover over a single comment in the row. Some option links will appear below the comment body.
  - If the comment appears to be legitimate, click "Approve"; the comment will now appear on the site.

## Spammy Form Responses

You will also find spammy form responses. These range from non-relevant advertising to "I want to help you make your site better" pitches. 

In `WP Admin -> Feedback`:

- Select the checkbox of each spammy form response
- At the top or bottom of the table, select "Mark as Spam" from the "Bulk Actions" menu and click "Apply"
