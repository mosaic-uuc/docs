# Pages

This will have instructions and workflows for maintaining specific pages and adding new ones.

Reference: [WordPress instructions for creating pages](https://wordpress.org/documentation/article/create-pages/)

Mosaic policy is to use the smallest possible number of pages to get the job done. Please do not add pages except when necessary.