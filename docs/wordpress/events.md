# Events Manager

This is how we add in-person, community and social justice events to the
events page.

## Add a New Event

1. Visit Events in the left hand menu.
2. Click add event in the upper left hand side.
3. Enter the Events's Name or Title in the "Title" section.
4. Paste or type the Event's details into the space where it says, "Type / to
choose a block".
5. In the "Where" Box, select the relevant location.
    1. If the event is online and the join URL is public, select "URL" and paste the join URL in the appropriate box.
    2. If the event is in a physical place, select "Physical Location". Begin typing the location name into the "Location Name" box. If there is already a location set up for that place, you should see it appear in the results after a few seconds. If it does not appear, you will need to create it. When you create a new event location, please make sure you deselect the "Allow Comments" option.
6. Locate the options box titled "When".
    1. Enter the date and time of the event into the field titled "When". 
7. Enter the event
category in the bottom left hand side of the screen. 
8. A parent or primary event is defined as a category in which entered 
subcategories relate. 
    1. Ex. Ceremony & Celebration is a parent category and Water Communion is a subcategory. 
9. If no event category exists from the available list you can add a new event. 
10. Locate the option box titled Featured image.
    1. Click set featured image.
    2. Choose which image you would like then click set featured image on the lower
right hand side.
11. Locate the box titled publish. Click publish.
