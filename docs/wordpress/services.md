# Services

The UUA Services plugin allows you to display upcoming services in a nice way.

## Add a New Service

Start from the Admin dashboard.

1. Visit "Services" in the left-hand menu.
2. Click "Add New". An Editor view will load.
3. Enter the Service's Name or Title in the "Title" section.
4. Paste or type the Service's details into the space where it says, "*Type / to choose a block*".
5. Locate the options box titled "*Service Date and Time*".
    1. Click inside the "*Service Date and Time*" field and use the date picker to enter the date. The field will display the date in the following format: `DD-MM-YYYY HH:MM AM/PM`
    2. Enter the Service date/time into the "*Display Date and Time*" field in the following format: `Month DD, YYYY HH am/pm`
6. Locate the options box titled "*Speakers*"
    1. Begin typing the first speaker's name in the Speakers form field. If WordPress already knows about a speaker, it will suggest their name. Add all people listed in the service description. Each may be separated by a comma or by pressing the enter key.
7. Locate the options box titled "*Topics*".
    1. Select any topics that apply. You may select both top-level and sub-level topics.
    2. Set the primary topic.
    3. If you can't find at least one existing topic that fits, create a new topic by clicking on "*Add New Topic*" and following the prompts.
8. Optional: if there is an image associated with the service, you may add it in the "*Service Image*" option box.
9. Scheduling
    1. Each Service should be scheduled to appear 5 Sundays before the Service's date (including that Sunday in the count).
    2. For example, a Service that will occur on July 17 would be published on June 19.
    3. If the publish date is in the future, find the Status & Visibility options box, and click "Immediately" (to the right of "Publish") to set the publication date to the correct date.
    4. Set the publish time for 12:01 am
10. Publish
    1. If a service does not have full details (i.e. the Service details say "TBA" or "TBD"), save it as a Draft and do not publish until those details have been entered.
    2. If a service's details are complete and scheduling is set up, click "*Publish*"
