# Mini Tasks

This page collects small tasks that don't belong to any particular area.

## Updating Links

1. Sign in to the Mosaic website.
2. Locate the page or post in which the link is in need of updating.
    1. Depending on the location, the actual document that contains the link may be difficult to locate. If you have trouble, please ask for help.
3. Click "Edit Page" in the top (near the center) portion of the screen.
4. Hover over the text in which the link is in need of updating. Click the text.
5. Delete the old link from the URL field and paste the new one in its place.
3. Press the "Enter" key.
4. Click "Update" in the upper right hand portion of the screen.

## Editing MetaSlider Slideshows

### Add a picture to an existing slideshow.

1. Sign in to the Mosaic website.
2. From the Admin Dashboard, find "Metaslider" in the left-hand navigation sidebar and click it.
3. Find the name of the slideshow you would like to edit and click it.
4. Click "Add Slide" located in the upper right hand side of the screen.
5. Choose the pictures you would like to add and follow the prompts.
6. Click "Save" located in the upper right hand side of the menu.

### Delete a picture from an existing slideshow

1. Sign in to the Mosaic website.
2. From the Admin Dashboard, find "Metaslider" in the left-hand navigation sidebar and click it.
3. Find the name of the slideshow you would like to edit and click it.
4. Locate the picture you would like to delete.
5. Click the "X" in the upper left menu on the picture itself.
6. Click "Save" located in the upper right hand side of the Editor menu.
